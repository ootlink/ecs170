import java.util.List;
import java.util.ArrayList;



public class SecondAI extends AIModule
{

	private int[] colScore;
	private int ourPlayer;
	private List<int[]> moves;
	private int[][] board;
	GameStateModule rootState;
	private boolean debug;

	// This is the major part of the evaluation function - the coin counter.
	// 
	// It can automatically detect to count downwards or upwards on columns or rows. 
	// It can even count diagonally! 
	//
	private int countCoins(int startCol, int endCol, int startRow, int endRow, final GameStateModule state)
	{
		// Variables. Mmm.
		//
		int coinAbove = ourPlayer;
		int numCoins = 0;
		int score = 0; 
		boolean countDownRows = false;
		boolean countDownCols = false;
		boolean countDiagonal = false;

		// Check if we're counting downwards on rows or cols
		//
		if (startRow > endRow)
			countDownRows = true;
		if (startCol > endCol)
			countDownCols = true;

		// Now check if we're counting diagonally. In that case everything's different. 
		// (note, upwards/downwards still applies. :))
		//
		if ((startRow != endRow) && (startCol != endCol))
		{
			countDiagonal = true;
		}

		// If we're counting diagonally, we use this value to know where we're going
		//
		int diagonalCount = startRow;

		// Step counter goes to 3. For good measure.
		//
		int numSteps = 0;


		// Start the outer loop. The outer loop iterates the columns from startCol to endCol.
		// (or stops when numSteps == 3)
		//
		int i = startCol;
		while (true)
		{

			if (debug) System.out.println("Col: " + i);

			// Start the inner loop. The inner loop iterates rows from startRow to endRow
			// UNLESS we're counting diagonally. In that case the inner loop only runs 
			// once and then decrements diagonalCount.
			//
			// I know it's ridiculous but it saves a lot of redundant code. Imagine how this
			// crap would look with 1000 lines lol.
			// 
			int j = (countDiagonal ? diagonalCount : startRow);
			while (true)
			{

				if (debug) System.out.println("Row: " + j);

				if (state.getAt(i,j) == 0)
				{
					// Since state.getAt returns 1 or 2 (valid players) or 0 (not found)
					// if it's not found we just ignore this round.
					// *whistles*
					//
				}
				else if (state.getAt(i,j) == coinAbove)
				{
					// YAY WE GOT A MATCH!!!!
					// 
				//	System.out.println("Found coin at (" + i + " , " + j + ")");

					// Increment stuff and add to the score. 
					//
					numCoins++;

					if (coinAbove == ourPlayer)
					{
						if (numCoins == 1)
							score = 1; 
						else if (numCoins == 2)
							score = 10;
						else if (numCoins == 3)
							score = 100;
						else
							break;
					}
					else
					{
						if (numCoins == 1)
							score = -1; 
						else if (numCoins == 2)
							score = -10;
						else if (numCoins == 3)
							score = -100;
						else
							break;
					}
				}

				// If it's an enemy coin, and it's the first coin we've seen yet...
				// Then we flip coinAbove and add to the score/numCoins.
				// 
				else if ( (state.getAt(i, j) != coinAbove) && (score == 0) )
				{
		//			System.out.println("Found enemy coin at (" + i + " , " + j + ")");
					// This is NOT ourPlayer. Keep that in mind for scoring.
					//
					coinAbove = state.getAt(i,j);
					score = -1;
					numCoins++;
				}

				// If it's an enemy coin but we just had a series of friendly coins,
				// break the loop because we just hit a dead end.
				//
				else
					break;


				// Iteration junk. 
				// If we're counting diagonally, just decrement the diagonalCount
				// and break the loop (it only runs once per column loop)
				//
				if (countDiagonal)
				{
					diagonalCount--;
					break;
				}
				// Otherwise, either we decrement our row or we increment it. 
				// That depends on voodoo, and whatever we specified (counting up to a limit or down to 0)
				//
				else
				{
					if (countDownRows)
					{
						j--;
						if (j < 0)
							break;
					}
					else
					{
						j++;
						if (j >= endRow)
							break;
					}
					if (numSteps == 3)
						break;
					else
						numSteps++;

				}

			} // end row loop

			// ---------------------------------------------------------------------
			// 
			// Phew. Back to the column loop. Here's a little separation to keep
			// you from going WTF IS THIS STUFF?! I swear I wrote this sober.
			// ---------------------------------------------------------------
			//
			if (numSteps == 3)
				break;
			else
			{
				numSteps++;
	//			System.out.println("numSteps = " + numSteps + " numCoins = " + numCoins);
			}

			if (diagonalCount < 0)
				break;

			// If we're counting up or down, just do whatever. 
			//
			if (countDownCols)
			{
				i--;
				if (i < 0)
					break;
			}
			else
			{
				i++;
				if (i >= endCol)
					break;
			}


		}

		// Oh look. we're done! Return stuff. 
		//
		return score;
	}



	// generateMoves populates the column array with scores. That's all it does.
	// 
	// 
	private int generateMoves(final GameStateModule state) 
	{
		int totalScore = 0;

		// get a copy of the state. root state! 
		rootState = state;

		// System.out.println("called generateMoves");

		// Let's keep this REAL simple. if you can use a column, it'll be checked
		for (int column = 0; column < state.getWidth(); column++)
		{

			if (state.canMakeMove(column))
			{
				// find out score for dropping a coin in this column
				//

				// First, find the column's score
				//
				int startRow = state.getHeightAt(column);
				int endRow = 0;
				int startCol = column;
				int endCol = column; 
				int score = 0;

				// countCoins counts down from startCount to endCount
				//
				score += countCoins(startCol, endCol, startRow, endRow, state);

				// Then add the left row score
				//
				endRow = startRow;
				if (column > 0)
				{
					endCol = 0;
					if (debug) System.out.println("Checking left rows");
					score += countCoins(startCol-1, endCol, startRow, endRow, state);
				}

				if (column < (state.getWidth() - 1))
				{
					// Then add the right row score
					startCol = column;
					endCol = state.getWidth();
					if (debug) System.out.println("Checking right rows");
					score += countCoins(startCol+1, endCol, startRow, endRow, state);
				}

				// let's try counting diagonally.
				// 

				if (debug) System.out.println("Checking Diagonally down/left");

				startCol = column; 
				endCol = column - 3;
				startRow = state.getHeightAt(column);
				endRow = startRow - 3;
				// going left 3 and down 3
				score += countCoins(startCol-1, endCol, startRow, endRow, state);

				// going right 3 and down 3
				endCol = column + 3;
				if (debug) System.out.println("Checking Diagonally down\\right");
				score += countCoins(startCol+1, endCol, startRow, endRow, state);

				// going right 3 and up 3
				endRow = startRow + 3;
				if (debug) System.out.println("Checking Diagonally up/right");
				score += countCoins(startCol, endCol, startRow, endRow, state);

				// going left 3 and up 3
				endCol = column - 3;
				if (debug) System.out.println("Checking Diagonally up\\left");
				score += countCoins(startCol, endCol, startRow, endRow, state);

				totalScore += score;

			}
		}

		// tally up the score:
		//
		// for (int j = 0; j < state.getWidth(); j++)
		// {
		// 	if (state.canMakeMove(j))
		// 	{
		// 		totalScore += scoreTable[j];
		// 	}
		// }

		return totalScore;
	}


	// Here's our beautiful main function - getNextMove(). Notice how incredibly simple it is.
	// Most of it is just debug printing, actually. 
	// 
	@Override
	public void getNextMove(final GameStateModule state)
	{
		// So minimax needs to predict what happens x moves ahead of time. 
		// 
		// Technically, we're the maximizing node. The maximizing node would pick the highest 
		// score. The minimizing node would pick the lowest score. 
		// 
		debug = false;

		GameStateModule states[] = new GameStateModule[state.getWidth()];

		int scores[];
		scores = new int[state.getWidth()];
		for (int k = 0; k < state.getWidth(); k++)
			scores[k] = 0;

		// The scores per round are in an array called colScore. 
		// 
		ourPlayer = state.getActivePlayer();
		System.out.println("----------------------------------------");
		System.out.println("our Player: " + ourPlayer);

		for (int i = 0; i < state.getWidth(); i++)
		{
			// make a copy of the root state
			//
			states[i] = state.copy();

			// play the ith column
			if (states[i].canMakeMove(i))
			{
				// Make our hypothetical move on the ith column
				// 
				states[i].makeMove(i);

				// Find out what score the state would get after the ith
				// move (this is the state our opponent will play)
				//
				scores[i] = generateMoves(states[i]);


			}

			// I think we can recursively call getNextMove to see what would be the next move after? 

			System.out.println("Scores[ " + i + "] = " + scores[i]);

		}

		// Pick the state with the lowest score
		//
		int bestScore = 999999;
		int secondBestScore = bestScore;
		for (int i = 0; i < state.getWidth(); i++)
		{
			if ((scores[i] < bestScore) && state.canMakeMove(i))
			{
				//chosenMove = i;
				secondBestScore = bestScore;
				bestScore = scores[i];
			}
			else if ((scores[i] < secondBestScore) && state.canMakeMove(i))
			{
				chosenMove = i;
				secondBestScore = scores[i];

			}
		}
		System.out.println("Chose column " + chosenMove);



	}



}