import java.util.List;
import java.util.ArrayList;

public class NewAI extends AIModule
{

	private int[] colScore;
	private int ourPlayer;
	private List<int[]> moves;
	private int[][] board;
	GameStateModule rootState;
	private boolean debug;

	// getCoinAt checks if the desired result is within bounds. If it is, it returns what 
	// coin is present. (0 if empty, 1 or 2, or -1 if not within bounds). 
	//
	private int getCoinAt(int x, int y, final GameStateModule state)
	{
		if ( (x >= 0) && (x < state.getWidth()) && (y >= 0) && (y < state.getHeight()) )
		{
		//	System.out.format("requested coin at (%d,%d) = %d\r\n", x, y, state.getAt(x,y));
			return state.getAt(x,y);
		}
		else
		{
		//	System.out.format("requested coin at (%d,%d) = -1\r\n", x, y);
			return -1;
		}
	}

	private int evaluateBoard(final GameStateModule state)
	{
		int thisScore = 0;
		int boardWidth = state.getWidth() -1;

		// these vars get used a lot
		int s1 = 0;
		int s2 = 0; 
		int ce = 0;
		int score = 0;
		int s1score = 0;
		int s2score = 0;
		int coin = 0;
		int cScores[] = new int[7];


		for (int column = 0; column < boardWidth; column++)
		{
		//	System.out.println("evaluateBoard called. Player: " + state.getActivePlayer());

			// We can evaluate a column's playable empty space by analyzing the 3 spaces to each side:
			// 
			//(0)  (1)  (2)
			//   o  o  o
			//    o o o
			//     ooo
			//   ooo#ooo  (3)
			//     ooo
			//    o o o
			//   o  o  o


			// oo       oo       oo
			//    oo    oo    oo
			//       oo oo oo
			// oo oo oo ## oo oo oo
			//
			// where # is the space we want to play and o's are spaces that may or may not be empty.
			//
			// Those spaces are either -1 (out of bounds), 0 (empty), 1 (player 1), or 2 (player 2)
			// 
			// This function analyzes lines 1, 2, 3, and 4 of length 7, from bottom to top. 
			// It then gives the column a score. 
			//
			// For each line, the column first counts up for player 1, then player 2.
			// 
			int lines[][] = new int[4][7];

			int x = column;
			int y = state.getHeightAt(column)-1;

			// Populate lines with this loop. :)
			//
			if (debug)
			{
				System.out.println("----------------------------");
				System.out.println("column: " + column);
			}
			for (int offset = -3; offset <= 3; offset++)
			{
//				System.out.format("offset = %d, x+offset = %d, x-offset = %d, y-offset = %d, y+offset = %d \r\n", offset, (x+offset), (x-offset), (y-offset), (y+offset));
				lines[0][offset+3] = getCoinAt(x-offset, y+offset, state);
				lines[1][offset+3] = getCoinAt(x, y+offset, state);
				lines[2][offset+3] = getCoinAt(x+offset, y+offset, state);
				lines[3][offset+3] = getCoinAt(x+offset, y, state);

				// System.out.println("[" + (offset+3) + "] " + lines[0][offset+3] + " " + lines[1][offset+3] + " " + lines[2][offset+3] );
			}

			if (debug)
			{
				System.out.format("[6] %2d       %2d       %2d      \r\n", lines[0][6], lines[1][6], lines[2][6]);
				System.out.format("[5]    %2d    %2d    %2d         \r\n", lines[0][5], lines[1][5], lines[2][5]);
				System.out.format("[4]       %2d %2d %2d            \r\n", lines[0][4], lines[1][4], lines[2][4]);
				System.out.format("[3] %2d %2d %2d %2d %2d %2d %2d  \r\n", lines[3][0], lines[3][1], lines[3][2], lines[3][3], lines[3][4], lines[3][5], lines[3][6]);
				System.out.format("[2]       %2d %2d %2d            \r\n", lines[2][2], lines[1][2], lines[0][2]);
				System.out.format("[1]    %2d    %2d    %2d         \r\n", lines[2][1], lines[1][1], lines[0][1]);
				System.out.format("[0] %2d       %2d       %2d      \r\n", lines[2][0], lines[1][0], lines[0][0]);
			}

			cScores[column] = 0;
			// Now count! For each line:
			//
			for (int[] line:lines)
			{
				s1 = 0;
				s2 = 0;
				ce = 0;
				s1score = 0;
				s2score = 0;
				// Count up for S1
				for (int row = 0; row < 7; row++)
				{
					if (line[row] == -1)
						continue;			// skip out-of-bounds cells

					if (line[row] == 1)
						s1++;
					else if (line[row] == 0)
						ce++;
					else if (line[row] == 2)
					{
						if ((s1+ce) >= 4)
							break;

						s1 = 0;
						ce = 0;
					}

					// if ((s1 + ce) == 4)
					// 	break;
				}


				// Tally up S1 score
				if (ce == 1)
					s1score = 0;
				else if ((s1 == 1) && (ce == 3))
					s1score += 1;
				else if ((s1 == 2) && (ce == 2))
					s1score += 10;
				else if ((s1 == 3) && (ce == 1))
					s1score += 100;

				if (debug) System.out.format("s1 = %d, ce = %d, s1score = %d", s1, ce, s1score);

				// Now do the same for s2
				// reset CE first 
				ce = 0;
				for (int row = 0; row < 7; row++)
				{
					if (line[row] == -1)
						continue;			// skip out-of-bounds cells

					if (line[row] == 2)
						s2++;
					else if (line[row] == 0)
						ce++;
					else if (line[row] == 1)
					{
						if ((s2+ce) >= 4)
							break;
						s2 = 0;
						ce = 0;
					}

					// if ((s2 + ce) == 4)
					// 	break;
				}

				// Tally up S2 score
				if (ce == 0)
					s2score = 0;
				else if ((s2 == 1) && (ce == 3))
					s2score += 1;
				else if ((s2 == 2) && (ce == 2))
					s2score += 10;
				else if ((s2 == 3) && (ce == 1))
					s2score += 100;

				if (debug) System.out.format(" s2 = %d, ce = %d, s2score = %d\r\n", s2, ce, s2score);

				if (ourPlayer == 1)
				{
					score += (s1score - s2score);
					cScores[column] = s1score - s2score;
				}
				else
				{
					score += (s2score - s1score); 
					cScores[column] = s2score - s1score;
				}

//				System.out.println("total score " + score);


			} // end line iterator
			if (debug) System.out.println("score[" + column + "] = " + cScores[column]);

		} // end column iterator

		if (debug)
		{
			for (int i = 0; i < 7; i++)
			{
//				System.out.println("score[" + i + "] = " + cScores[i]);
			}

			System.out.println("Total score: " + score);
		}
//		System.out.println("evaluateBoard ended. Player: " + state.getActivePlayer() + " score: " + thisScore);
		return score;
	}

	// get an array of valid column choices.
	//
	private List<Integer> generateMoves(final GameStateModule state)
	{
		List<Integer> moves = new ArrayList<Integer>();
	
		if (state.isGameOver())
			return moves;

		for (int i = 0; i < state.getWidth(); i++)
		{
			if (state.canMakeMove(i))
				moves.add(i);
		//	System.out.println("valid move: " + i);
		}

		return moves;
	}

	// minimax returns [score, col] 
	private int[] minimax(int depth, final GameStateModule state)
	{	
	//	System.out.println("Minimax called. Player: " + state.getActivePlayer() + " depth: " + depth);

		List<Integer> nextMoves = generateMoves(state);

		int bestScore = 0; 

		if (state.getActivePlayer() == ourPlayer)
			bestScore = Integer.MIN_VALUE;
		else
			bestScore = Integer.MAX_VALUE;

		int currentScore = 0;
		// int bestColumn = 0;
		int count = 0;

		if (state.isGameOver() || (depth == 0))
		{
			bestScore = evaluateBoard(state);
	//		System.out.println("Minimax done. Score = " + bestScore);
		}
		else
		{
			for (int move:nextMoves)
			{
			//	System.out.format("count = %d", count);
				count++;
				int newDepth = depth -1;
				GameStateModule newState = state.copy();
				newState.makeMove(move);
				if (newState.getActivePlayer() == ourPlayer)
				{
					currentScore = minimax(newDepth, newState)[0];
					if (currentScore < bestScore)
					{
						bestScore = currentScore;
						chosenMove = move; // bestColumn = move;
				//		System.out.println("ChosenMove: " + chosenMove);
					}
				}
				else
				{
					currentScore = minimax(newDepth, newState)[0];
					if (currentScore > bestScore)
					{
						bestScore = currentScore;
						chosenMove = move; //bestColumn = move;
				//		System.out.println("ChosenMove: " + chosenMove);
					}

				}

			//	System.out.format("bestScore = %d\r\n", bestScore);

				// System.out.println("Minimax ended." + depth +  " Player: " + newState.getActivePlayer() + " score: " + bestScore + " column: " + chosenMove);
			}
		}

		// System.out.println("Minimax ended. Player: " + newState.getActivePlayer() + " score: " + bestScore + " column: " + chosenMove);

		return new int[] {bestScore, chosenMove};
	}

	// Here's our beautiful main function - getNextMove(). Notice how incredibly simple it is.
	// Most of it is just debug printing, actually. 
	// 
	@Override
	public void getNextMove(final GameStateModule state)
	{
		// So minimax needs to predict what happens x moves ahead of time. 
		// 
		// Technically, we're the maximizing node. The maximizing node would pick the highest 
		// score. The minimizing node would pick the lowest score. 
		// 
		GameStateModule states[] = new GameStateModule[state.getWidth()];
		int scores[] = new int[state.getWidth()];
		debug = false;

		// The scores per round are in an array called colScore. 
		// 
		ourPlayer = state.getActivePlayer();

		System.out.println("----------------------------------------");

		System.out.println("our Player: " + ourPlayer);

		 chosenMove = minimax(3, state)[1];

	// 	for (int i = 0; i < state.getWidth(); i++)
	// 	{
	// 		// make a copy of the root state
	// 		//
	// 		states[i] = state.copy();

	// 		// play the ith column
	// 		if (states[i].canMakeMove(i))
	// 		{
	// 			// Make our hypothetical move on the ith column
	// 			// 
	// 			states[i].makeMove(i);

	// 			// Find out what score the state would get after the ith
	// 			// move (this is the state our opponent will play)
	// 			//

	// 			System.out.println("if we play column " + i);

	// 			// scores[i] = generateMoves(states[i]);
	// 			scores[i] = evaluateBoard(states[i]);


	// 		}

	// 		// I think we can recursively call getNextMove to see what would be the next move after? 

	// //		System.out.println("Scores[ " + i + "] = " + scores[i]);

	// 	}
		// Pick the state with the lowest score
		//
		// int bestScore = -999999;
		// int secondBestScore = bestScore;
		// for (int i = 0; i < state.getWidth(); i++)
		// {
		// 	if ((scores[i] > bestScore) && state.canMakeMove(i))
		// 	{
		// 		chosenMove = i;
		// 		bestScore = scores[i];
		// 	}
		// }
		// System.out.println("Chose column " + chosenMove);

		// update the list of moves


		// int worstScore = 999999;
		// int bestColumn = 0;

		// // Now, minimax stipulates that we need to look ahead, sooo what do we do?

		// // Pick the column with the highest score.
		// for (int i = 0; i < state.getWidth(); i++)
		// {
		// 	if (state.canMakeMove(i) && (colScore[i] < worstScore) )
		// 	{
		// 		worstScore = colScore[i];
		// 		bestColumn = i;
		// 	}
		// }

		// chosenMove = bestColumn;

		// System.out.println("Played column: " + chosenMove);

	}



}