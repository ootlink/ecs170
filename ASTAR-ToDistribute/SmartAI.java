// Joseph Cranmer & William Freeman
// 996914530 & 997714917
// ECS 170 Project 1

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


/// Base class of all AI implementations
/**
 * An interface representing a pathfinder AI interface.  The interface exports
 * a single function (createPath) which accepts as input a TerrainMap and computes
 * a path from the start location to the end location.
 * 
 * @author Leonid Shamis
 * @see TerrainMap
 */
 
 
public class SmartAI implements AIModule
{

	// getH does a heuristic calculation of the straight line distance from src to dest.
	//
	public double getH (final TerrainMap map, Point src)
	{	
	
		Point dest = map.getEndPoint();
		
		// There has to be some sort of influence on the distance from the current point to the destination. There just has to be.
		//
		double distance = src.distance(dest);
		
		return distance;
	}
	
	
	// asNode is an extension of the Points we're given. 
	// It includes g (total cost from start point to this node) and
	// 				  f  (total cost from start point to this node + heuristic)
	// as well as a link back to the parent
	// which helps for the reconstructing step at the end.
	//
	class asNode implements Comparable<asNode>
	{
		Point location;
		asNode parent;
		double g;
		double f;
		
		// constructors. Boring! 
		//
		public asNode () {
			this.location = null;
			this.parent = null;
			this.g = 0;
			this.f = 0;
		}
		
		public asNode (Point loc) { 
        	this.location = loc; 
        	this.parent = null; 
			this.g = 0;
			this.f = 0;
	    } 

		
		public asNode (Point location, asNode parent, double g, double f) { 
        	this.location = location; 
        	this.parent = parent; 
			this.g = g;
			this.f = f;
	    } 
		
		public asNode (asNode thisNode)
		{
        	this.location = thisNode.location; 
        	this.parent = thisNode.parent; 
			this.g = thisNode.g;
			this.f = thisNode.f;
		}
		
		// This allows us to get the item in the list with the smallest f
		//
		@Override
		public int compareTo(asNode other){
			if(this.f != other.f)
			{
			  return Double.compare(this.f, other.f);    //sorting in ascending order.
			 }
			 return 0;
		}
	}
	
	// reconstruct path is called at the end to trace all the way back to the start node from the 
	// end node by using parents. Kinda like a linked list. :) 
	// 
	private ArrayList<Point> reconstruct_path(asNode currentNode)
	{
		ArrayList<Point> result = new ArrayList<Point>();

		while (currentNode != null) {
			result.add(currentNode.location);
			currentNode = currentNode.parent;
		}
						
		// reverse path since we've got a backwards path from end to source right now
		// 
		Collections.reverse(result);

		return result;
	}
	
	
	// The big function we're supposed to be working on :D
	//
    public List<Point> createPath(final TerrainMap map)
    {
        // Holds the resulting path
        final ArrayList<Point> path = new ArrayList<Point>();
				        
        final ArrayList<asNode> asOpenList = new ArrayList<asNode>();
        final ArrayList<asNode> asClosedList = new ArrayList<asNode>();


        // Keep track of where we are and add the start point.
        Point currentPoint = map.getStartPoint();
		
		asNode currentNode = new asNode (map.getStartPoint(), null, 0, 0);
		
		boolean finished = false;
		
		asOpenList.add(currentNode);
		
				
 		System.out.println("Start Point: x: " + currentNode.location.x + " y: " + currentNode.location.y);		
		System.out.println("End Point: x: " + map.getEndPoint().x + " y: " + map.getEndPoint().y);		

		// While Openset is NOT empty
		
		while ((asOpenList.size() > 0))
		{

			// Find the node with the smallest .f = g+h
			//
			int minIndex = asOpenList.indexOf(Collections.min(asOpenList));
			currentNode = asOpenList.get(minIndex);
			
			// If we've found the goal, we're done.
			//
			if (currentNode.location.equals(map.getEndPoint()))
			{
				System.out.println("FOUND IT!!!!!!!!!");
				return reconstruct_path(currentNode);
			}
			

			// Remove currentNode from open set, stick in closed set
			//
			asClosedList.add(currentNode);	
			asOpenList.remove(currentNode);
			

			// Now we look at each of its neighbors. 
			//
			Point[] neighbors = map.getNeighbors(currentNode.location);
			
			
			// Remember, thisCost = cost of last point in path -> current Point 
			//
			for (Point neighbor : neighbors)
			{
			
				// System.out.println("Examining Neighbor Point: x: " + neighbor.x + " y: " + neighbor.y);		

				// Check if the neighbor is in the closed set
				//
				int foundInClosedList = 0;
				for (asNode closedListItem : asClosedList)
				{
					if (closedListItem.location.equals(neighbor))
					{
						foundInClosedList = 1;
					}
				}
				
				// If it's in the closed list, skip it.
				//
				if (foundInClosedList != 0)
				{
					continue;
				}
				else
				{
					// If it's not in the closed list, check if it's in the open set: 
					//
					
					
					// Now check if that neighbor exists in the open list
					//
					int indexInOpenList = -1;
					for (asNode openListItem : asOpenList)
					{
						if (openListItem.location.equals(neighbor))
						{
							indexInOpenList = asOpenList.indexOf(openListItem);
						}
					}

					
					//  but first, get the cost
					double neighbor_g = currentNode.g + map.getCost(currentNode.location, neighbor); 
					double neighbor_f = neighbor_g + getH(map, neighbor);

					// Now then, let's check if it's in the open set..
					// 
					if (indexInOpenList > -1)
					{
						// It is? If we have a better cost, change the parent to currentPoint
						//
						if (neighbor_f < asOpenList.get(indexInOpenList).f)
						{
							asOpenList.remove(indexInOpenList);
							asNode newNeighbor = new asNode (neighbor, currentNode, neighbor_g, neighbor_f);
							asOpenList.add(newNeighbor);

						}
						else
						{
							continue;
							
						}
					}
					else // not found in openList, add it!
					{
							asNode newNeighbor = new asNode (neighbor, currentNode, neighbor_g, neighbor_f);
							asOpenList.add(newNeighbor);
					}
				}
				
			}
			
		}
		
		

        // We're done!  Hand it back.
        return path;
    }
}