// Joseph Cranmer & William Freeman
// 996914530 & 997714917
// ECS 170 Project 1

Unfortunately, we were only able to come up with the one (straight line/euclidean) heuristic, and spent most of our time trying to get the A* algorithm to work without a heuristic (which we haven't had any luck with) - as it is, it should be admissible because in the absolute minimal case, the cost from all points to all other points would be the same, and then the shortest distance between the start point and end point would be the shortest distance. 

Anyway, because of that, for part 1 we only have 1 .java file with the one heuristic (in the getH function). 